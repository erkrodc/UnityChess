﻿using UnityChess;
using UnityEngine;

[CreateAssetMenu]
public class MoveHistory : RuntimeStack<Movement> {

}