﻿namespace UnityChess {
    /// <summary>
    ///     Common ancestor of Piece and EmptyPiece/InvalidPiece.
    /// </summary>
    public abstract class BasePiece { }
}