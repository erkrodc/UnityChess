﻿namespace UnityChess {
    /// <summary>
    ///     Representation of a valid, unoccupied square on the board.
    /// </summary>
    public class EmptyPiece : BasePiece { }
}