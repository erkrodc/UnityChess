﻿namespace UnityChess {
    /// <summary>
    ///     Representation of those squares that are outside of the
    ///     8x8 center of the 10x12 grid being used to represent a chessboard.
    /// </summary>
    public class InvalidPiece : BasePiece { }
}