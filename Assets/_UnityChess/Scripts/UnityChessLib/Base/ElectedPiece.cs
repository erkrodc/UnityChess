﻿namespace UnityChess {
	public enum ElectedPiece {
		Knight,
		Bishop,
		Rook,
		Queen
	}
}