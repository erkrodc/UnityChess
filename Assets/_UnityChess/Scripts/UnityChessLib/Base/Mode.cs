﻿namespace UnityChess {
	/// <summary>
	///     Describes whether each player is either a Human or AI
	/// </summary>
	public enum Mode {
		HvH,
		HvA,
		AvA
	}
}