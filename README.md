# UnityChess

UnityChess is a 3D Chess game built using Unity/C#, complete with its own AI.

![UnityChess demo](https://i.imgur.com/VJOXHkK.jpg)

### Contributing
Be sure to follow this [branching strategy](https://docs.google.com/document/d/15klJI6eCww5gX8QA6ZMac-jmPLpe1CN79f1v6vGPkNw/pub) if you were given push priveleges. Otherwise, follow the collaborative github workflow, well described [here](https://github.com/asmeurer/git-workflow/blob/master/README.md), and make your branch off of the development branch.


If you wish to contribute, it is recommended you have the following resources:

* [Unity 2017.3.1f1](https://unity3d.com/get-unity/download/archive) - A free to download game engine.
* [Visual Studio Community 2017](https://www.visualstudio.com/downloads/) - Microsoft's free IDE for C# (amongst other languages)
* [Gitkraken](https://www.gitkraken.com/download) - A git/github interface that makes it easy to rebase and view branch histories

### Installation

At the moment, there have been no releases. The project is still in the early stages of development.
